#!/usr/bin/env python
from os import path
from setuptools import setup, find_packages

from PlSpiderCloud import __version__,__author__

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'requirements.txt')) as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]

setup(
    name='PlSpiderCloud',
    version=__version__,
    description='Admin ui for pl spider service',
    author=__author__,
    author_email='mahmood.bayeshi@ebuynow.com',
    packages=find_packages(),
    install_requires=install_requires,
    include_package_data=True,
    zip_safe=False,

    entry_points={
        'console_scripts': {
            'plspidercloud = PlSpiderCloud.run:main'
        },
    },

    classifiers=[
        'Development Status :: 1 - Beta',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
    ],
)
